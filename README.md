# ci-shenanigans

Repository for testing and demonstrating [dynamic child pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#trigger-a-dynamic-child-pipeline) in GitLab CI.

The second pipeline generator creates artifacts that are shared with the child pipeline jobs, using the method described in this issue comment: https://gitlab.com/gitlab-org/gitlab/-/issues/213457#note_1129225239

The pipeline and job names are meant to match the build jobs for the [osbuild/images](https://github.com/osbuild/images) project.

You can run the [scripts](scripts) in this repository to see what the dynamic pipeline configurations look like.
